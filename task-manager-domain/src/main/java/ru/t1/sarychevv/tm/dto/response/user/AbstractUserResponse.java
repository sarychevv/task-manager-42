package ru.t1.sarychevv.tm.dto.response.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.dto.model.UserDTO;
import ru.t1.sarychevv.tm.dto.response.AbstractResultResponse;

@Getter
@Setter
@NoArgsConstructor
public class AbstractUserResponse extends AbstractResultResponse {

    @Getter
    @Nullable
    private UserDTO user;

    @Getter
    @Nullable
    private String token;

    public AbstractUserResponse(@Nullable final String token) {
        this.token = token;
    }

    public AbstractUserResponse(@Nullable final UserDTO user) {
        this.user = user;
    }

    public AbstractUserResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}

package ru.t1.sarychevv.tm.repository;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.sarychevv.tm.api.repository.ISessionRepository;
import ru.t1.sarychevv.tm.api.service.IConnectionService;
import ru.t1.sarychevv.tm.api.service.IPropertyService;
import ru.t1.sarychevv.tm.dto.model.SessionDTO;
import ru.t1.sarychevv.tm.marker.DBCategory;
import ru.t1.sarychevv.tm.service.ConnectionService;
import ru.t1.sarychevv.tm.service.PropertyService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Category(DBCategory.class)
public class SessionRepositoryTest {

    private static final Integer NUMBER_OF_ENTRIES = 10;

    private static final String USER_ID_1 = UUID.randomUUID().toString();

    private static final String USER_ID_2 = UUID.randomUUID().toString();

    private static final String TEST_NAME = "Test session";

    private static final String TEST_DESCRIPTION = "Test description";
    @NotNull
    private final IPropertyService propertyService = new PropertyService();
    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);
    @NotNull
    private final SqlSession sqlSession = connectionService.getSqlSession();
    @NotNull
    private ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
    @NotNull
    private List<SessionDTO> sessionList;

    @Before
    public void initRepository() throws Exception {
        sessionList = new ArrayList<>();
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull SessionDTO session = new SessionDTO();
            session.setName(TEST_NAME + i);
            session.setDescription("description" + i);
            if (i <= 5) session.setUserId(USER_ID_1);
            else session.setUserId(USER_ID_2);
            sessionRepository.add(session);
            sessionList.add(session);
        }
    }

    @After
    public void removeRepository() throws Exception {
        sessionRepository.removeAll(USER_ID_1);
        sessionRepository.removeAll(USER_ID_2);
    }

    @Test
    public void testAdd() throws Exception {
        Integer expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final String userId = UUID.randomUUID().toString();
        @NotNull SessionDTO session = new SessionDTO();
        session.setName(TEST_NAME);
        session.setDescription(TEST_DESCRIPTION);
        sessionRepository.add(session);
        Assert.assertEquals(Optional.of(expectedNumberOfEntries), sessionRepository.getSize(USER_ID_1) + sessionRepository.getSize(USER_ID_2));
        @Nullable final SessionDTO actualSession = sessionRepository.findOneById(userId, session.getId());
        Assert.assertNotNull(actualSession);
        Assert.assertEquals(TEST_NAME, actualSession.getName());
        Assert.assertEquals(TEST_DESCRIPTION, actualSession.getDescription());
        Assert.assertEquals(userId, actualSession.getUserId());
    }

    @Test
    public void testAddSession() throws Exception {
        Integer expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final SessionDTO newSession = new SessionDTO();
        newSession.setName(TEST_NAME + expectedNumberOfEntries);
        newSession.setDescription(TEST_DESCRIPTION + expectedNumberOfEntries);
        sessionRepository.add(newSession);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize(USER_ID_1));
    }

    @Test
    public void testFindAll() throws Exception {
        Assert.assertEquals(NUMBER_OF_ENTRIES, (Integer) sessionRepository.findAll().size());
    }

    @Test
    public void testFindAllForUser() throws Exception {
        Assert.assertEquals(NUMBER_OF_ENTRIES - sessionRepository.findAll(USER_ID_2).size(),
                sessionRepository.findAll(USER_ID_1).size());
    }

    @Test
    public void testFindOneById() throws Exception {
        @NotNull final String wrongId = "501";
        @NotNull SessionDTO session = new SessionDTO();
        session.setName(TEST_NAME);
        session.setDescription(TEST_DESCRIPTION);
        sessionRepository.add(session);
        Assert.assertEquals(session, sessionRepository.findOneById(null, session.getId()));
        Assert.assertNotEquals(session, sessionRepository.findOneById(null, wrongId));
    }

    @Test
    public void testFindOneByIdForUser() throws Exception {
        @NotNull final String wrongId = "501";
        @NotNull SessionDTO session = new SessionDTO();
        session.setName(TEST_NAME);
        session.setDescription(TEST_DESCRIPTION);
        sessionRepository.add(USER_ID_1, session);
        Assert.assertEquals(session, sessionRepository.findOneById(USER_ID_1, session.getId()));
        Assert.assertNotEquals(session, sessionRepository.findOneById(USER_ID_2, session.getId()));
        Assert.assertNotEquals(session, sessionRepository.findOneById(USER_ID_1, wrongId));
        Assert.assertNotEquals(session, sessionRepository.findOneById(USER_ID_2, wrongId));
    }

    @Test
    public void testFindOneByIndex() throws Exception {
        @NotNull SessionDTO session = new SessionDTO();
        session.setName(TEST_NAME);
        session.setDescription(TEST_DESCRIPTION);
        sessionRepository.add(USER_ID_1, session);
        Assert.assertEquals(session, sessionRepository.findOneByIndex(USER_ID_1, NUMBER_OF_ENTRIES));
        Assert.assertNotEquals(session, sessionRepository.findOneByIndex(USER_ID_1, NUMBER_OF_ENTRIES - 1));
    }

    @Test
    public void testFindOneByIndexForUser() throws Exception {
        @NotNull SessionDTO session = new SessionDTO();
        session.setName(TEST_NAME);
        session.setDescription(TEST_DESCRIPTION);
        sessionRepository.add(USER_ID_1, session);
        Assert.assertEquals(session,
                sessionRepository.findOneByIndex(USER_ID_1, sessionRepository.findAll(USER_ID_1).size() - 1));
        Assert.assertNotEquals(session,
                sessionRepository.findOneByIndex(USER_ID_2, sessionRepository.findAll(USER_ID_2).size() - 1));
        Assert.assertNotEquals(session,
                sessionRepository.findOneByIndex(USER_ID_1, sessionRepository.findAll(USER_ID_1).size() - 2));
        Assert.assertNotEquals(session,
                sessionRepository.findOneByIndex(USER_ID_2, sessionRepository.findAll(USER_ID_2).size() - 2));
    }

    @Test
    public void testGetSize() throws Exception {
        Assert.assertEquals(NUMBER_OF_ENTRIES, sessionRepository.getSize(USER_ID_1));
        Assert.assertEquals(sessionList.size(), sessionRepository.getSize(USER_ID_1).intValue());
    }

    @Test
    public void testGetSizeForUser() throws Exception {
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2 + 1, Optional.of(sessionRepository.getSize(USER_ID_1)));
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2 - 1, Optional.of(sessionRepository.getSize(USER_ID_2)));
    }

    @Test
    public void testRemoveOne() throws Exception {
        @NotNull SessionDTO session = new SessionDTO();
        session.setName(TEST_NAME);
        session.setDescription(TEST_DESCRIPTION);
        sessionRepository.add(session);
        Assert.assertEquals(session, sessionRepository.removeOne(session));
        Assert.assertNull(sessionRepository.findOneById(session.getId()));
    }

    @Test
    public void testRemoveOneForUser() throws Exception {
        @NotNull SessionDTO session = new SessionDTO();
        session.setName(TEST_NAME);
        session.setDescription(TEST_DESCRIPTION);
        sessionRepository.add(USER_ID_1, session);
        Assert.assertEquals(session, sessionRepository.removeOne(session));
        Assert.assertNull(sessionRepository.findOneById(USER_ID_1, session.getId()));
        Assert.assertNull(sessionRepository.findOneById(session.getId()));
    }

    @Test
    public void testRemoveOneById() throws Exception {
        @NotNull SessionDTO session = new SessionDTO();
        session.setName(TEST_NAME);
        session.setDescription(TEST_DESCRIPTION);
        sessionRepository.add(session);
        Assert.assertEquals(session, sessionRepository.removeOneById(session.getId()));
        Assert.assertNull(sessionRepository.findOneById(session.getId()));
    }

    @Test
    public void testRemoveOneByIdForUser() throws Exception {
        @NotNull SessionDTO session = new SessionDTO();
        session.setName(TEST_NAME);
        session.setDescription(TEST_DESCRIPTION);
        sessionRepository.add(USER_ID_1, session);
        Assert.assertEquals(session, sessionRepository.removeOneById(USER_ID_1, session.getId()));
        Assert.assertNull(sessionRepository.findOneById(USER_ID_1, session.getId()));
        Assert.assertNull(sessionRepository.findOneById(session.getId()));
    }

    @Test
    public void testRemoveOneByIndex() throws Exception {
        @NotNull SessionDTO session = new SessionDTO();
        session.setName(TEST_NAME);
        session.setDescription(TEST_DESCRIPTION);
        sessionRepository.add(session);
        Assert.assertEquals(session, sessionRepository.removeOneByIndex(null, NUMBER_OF_ENTRIES));
        Assert.assertNull(sessionRepository.findOneById(session.getId()));
    }

    @Test
    public void testRemoveOneByIndexForUser() throws Exception {
        @NotNull SessionDTO session = new SessionDTO();
        session.setName(TEST_NAME);
        session.setDescription(TEST_DESCRIPTION);
        sessionRepository.add(USER_ID_1, session);
        Assert.assertEquals(session, sessionRepository.removeOneByIndex(USER_ID_1, NUMBER_OF_ENTRIES / 2 + 1));
        Assert.assertNull(sessionRepository.findOneById(session.getId()));
    }

    @Test
    public void testRemoveAllForUser() throws Exception {
        @NotNull final List<SessionDTO> emptyList = new ArrayList<>();
        sessionRepository.removeAll(USER_ID_1);
        Assert.assertEquals(emptyList, sessionRepository.findAll(USER_ID_1));
        Assert.assertNotEquals(emptyList, sessionRepository.findAll(USER_ID_2));
    }

    @Test
    public void testRemoveAll() throws Exception {
        final int expectedNumberOfEntries = 0;
        sessionRepository.removeAll(USER_ID_1);
        Assert.assertEquals(expectedNumberOfEntries, (long) sessionRepository.getSize(USER_ID_1));
    }

}

package ru.t1.sarychevv.tm.repository;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.sarychevv.tm.api.repository.IProjectRepository;
import ru.t1.sarychevv.tm.api.service.IConnectionService;
import ru.t1.sarychevv.tm.api.service.IPropertyService;
import ru.t1.sarychevv.tm.comparator.CreatedComparator;
import ru.t1.sarychevv.tm.dto.model.ProjectDTO;
import ru.t1.sarychevv.tm.enumerated.ProjectSort;
import ru.t1.sarychevv.tm.marker.DBCategory;
import ru.t1.sarychevv.tm.service.ConnectionService;
import ru.t1.sarychevv.tm.service.PropertyService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Category(DBCategory.class)
public class ProjectRepositoryTest {

    private static final Integer NUMBER_OF_ENTRIES = 10;

    private static final String USER_ID_1 = UUID.randomUUID().toString();

    private static final String USER_ID_2 = UUID.randomUUID().toString();

    private static final String TEST_NAME = "Test project";

    private static final String TEST_DESCRIPTION = "Test description";
    @NotNull
    private final IPropertyService propertyService = new PropertyService();
    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);
    @NotNull
    private final SqlSession sqlSession = connectionService.getSqlSession();
    @NotNull
    private IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
    @NotNull
    private List<ProjectDTO> projectList;
    ;

    @Before
    public void initRepository() throws Exception {
        projectList = new ArrayList<>();
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull ProjectDTO project = new ProjectDTO();
            project.setName(TEST_NAME + i);
            project.setDescription(TEST_DESCRIPTION + i);
            if (i <= 5) project.setUserId(USER_ID_1);
            else project.setUserId(USER_ID_2);
            projectRepository.add(project);
            projectList.add(project);
        }
    }

    @After
    public void removeRepository() throws Exception {
        projectRepository.removeAll(USER_ID_1);
        projectRepository.removeAll(USER_ID_2);
    }

    @Test
    public void testAdd() throws Exception {
        Integer expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final String userId = UUID.randomUUID().toString();
        @NotNull ProjectDTO project = new ProjectDTO();
        project.setName(TEST_NAME);
        project.setDescription(TEST_DESCRIPTION);
        projectRepository.add(userId, project);
        Assert.assertEquals(Optional.of(expectedNumberOfEntries), projectRepository.getSize(USER_ID_1));
        @Nullable final ProjectDTO actualProject = projectRepository.findOneById(userId, project.getId());
        Assert.assertNotNull(actualProject);
        Assert.assertEquals(TEST_NAME, actualProject.getName());
        Assert.assertEquals(TEST_DESCRIPTION, actualProject.getDescription());
        Assert.assertEquals(userId, actualProject.getUserId());
    }

    @Test
    public void testAddProject() throws Exception {
        Integer expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final ProjectDTO newProject = new ProjectDTO();
        newProject.setName(TEST_NAME + expectedNumberOfEntries);
        newProject.setDescription(TEST_DESCRIPTION + expectedNumberOfEntries);
        projectRepository.add(newProject);
        Assert.assertEquals(Optional.of(expectedNumberOfEntries), projectRepository.getSize(USER_ID_1));
    }

    @Test
    public void testFindAll() throws Exception {
        Assert.assertEquals(NUMBER_OF_ENTRIES, (Integer) (projectRepository.findAll(USER_ID_1).size() +
                projectRepository.findAll(USER_ID_2).size()));
    }

    @Test
    public void testFindAllWithComparator() throws Exception {
        @NotNull final String sortType = "BY_STATUS";
        @Nullable final ProjectSort sort = ProjectSort.toSort(sortType);
        if (sort != null) {
            Assert.assertEquals(sortType, ProjectSort.BY_STATUS.toString());
            Assert.assertEquals(NUMBER_OF_ENTRIES, projectRepository.findAll(String.valueOf(CreatedComparator.INSTANCE)));
        }
    }

    @Test
    public void testFindAllForUser() throws Exception {
        Assert.assertEquals(NUMBER_OF_ENTRIES - projectRepository.findAll(USER_ID_2).size(),
                projectRepository.findAll(USER_ID_1).size());
    }

    @Test
    public void testFindAllForUserWithComparator() throws Exception {
        @NotNull final String sortType = "BY_STATUS";
        @Nullable final ProjectSort sort = ProjectSort.toSort(sortType);
        if (sort != null) {
            @Nullable final List<ProjectDTO> projectList = projectRepository.findAll(String.valueOf(CreatedComparator.INSTANCE));
            Assert.assertEquals(sortType, ProjectSort.BY_STATUS.toString());
            if (projectList == null) return;
            Assert.assertEquals(NUMBER_OF_ENTRIES / 2 + 1, projectList.size());
        }
    }

    @Test
    public void testFindOneById() throws Exception {
        @NotNull final String wrongId = "501";
        @NotNull ProjectDTO project = new ProjectDTO();
        project.setName(TEST_NAME);
        project.setDescription(TEST_DESCRIPTION);
        projectRepository.add(USER_ID_1, project);
        Assert.assertEquals(project, projectRepository.findOneById(USER_ID_1, project.getId()));
        Assert.assertNotEquals(project, projectRepository.findOneById(USER_ID_1, wrongId));
    }

    @Test
    public void testFindOneByIdForUser() throws Exception {
        @NotNull final String wrongId = "501";
        @NotNull ProjectDTO project = new ProjectDTO();
        project.setName(TEST_NAME);
        project.setDescription(TEST_DESCRIPTION);
        projectRepository.add(USER_ID_1, project);
        Assert.assertEquals(project, projectRepository.findOneById(USER_ID_1, project.getId()));
        Assert.assertNotEquals(project, projectRepository.findOneById(USER_ID_2, project.getId()));
        Assert.assertNotEquals(project, projectRepository.findOneById(USER_ID_1, wrongId));
        Assert.assertNotEquals(project, projectRepository.findOneById(USER_ID_2, wrongId));
    }

    @Test
    public void testFindOneByIndex() throws Exception {
        @NotNull ProjectDTO project = new ProjectDTO();
        project.setName(TEST_NAME);
        project.setDescription(TEST_DESCRIPTION);
        projectRepository.add(USER_ID_1, project);
        Assert.assertEquals(project, projectRepository.findOneByIndex(USER_ID_1, NUMBER_OF_ENTRIES));
        Assert.assertNotEquals(project, projectRepository.findOneByIndex(USER_ID_1, NUMBER_OF_ENTRIES - 1));
    }

    @Test
    public void testFindOneByIndexForUser() throws Exception {
        @NotNull ProjectDTO project = new ProjectDTO();
        project.setName(TEST_NAME);
        project.setDescription(TEST_DESCRIPTION);
        projectRepository.add(USER_ID_1, project);
        Assert.assertEquals(project,
                projectRepository.findOneByIndex(USER_ID_1, projectRepository.findAll(USER_ID_1).size() - 1));
        Assert.assertNotEquals(project,
                projectRepository.findOneByIndex(USER_ID_2, projectRepository.findAll(USER_ID_2).size() - 1));
        Assert.assertNotEquals(project,
                projectRepository.findOneByIndex(USER_ID_1, projectRepository.findAll(USER_ID_1).size() - 2));
        Assert.assertNotEquals(project,
                projectRepository.findOneByIndex(USER_ID_2, projectRepository.findAll(USER_ID_2).size() - 2));
    }

    @Test
    public void testGetSize() throws Exception {
        Assert.assertEquals(Optional.of(NUMBER_OF_ENTRIES), projectRepository.getSize(USER_ID_1));
        Assert.assertEquals(projectList.size(), projectRepository.getSize(USER_ID_1));
    }

    @Test
    public void testGetSizeForUser() throws Exception {
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2 + 1, projectRepository.getSize(USER_ID_1));
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2 - 1, projectRepository.getSize(USER_ID_2));
    }

    @Test
    public void testRemoveOne() throws Exception {
        @NotNull ProjectDTO project = new ProjectDTO();
        project.setName(TEST_NAME);
        project.setDescription(TEST_DESCRIPTION);
        projectRepository.add(USER_ID_1, project);
        Assert.assertEquals(project, projectRepository.removeOne(USER_ID_1, project));
        Assert.assertNull(projectRepository.findOneById(USER_ID_1, project.getId()));
    }

    @Test
    public void testRemoveOneForUser() throws Exception {
        @NotNull ProjectDTO project = new ProjectDTO();
        project.setName(TEST_NAME);
        project.setDescription(TEST_DESCRIPTION);
        projectRepository.add(USER_ID_1, project);
        Assert.assertEquals(project, projectRepository.removeOne(USER_ID_1, project));
        Assert.assertNull(projectRepository.findOneById(USER_ID_1, project.getId()));
        Assert.assertNull(projectRepository.findOneById(USER_ID_1, project.getId()));
    }

    @Test
    public void testRemoveOneById() throws Exception {
        @NotNull ProjectDTO project = new ProjectDTO();
        project.setName(TEST_NAME);
        project.setDescription(TEST_DESCRIPTION);
        projectRepository.add(USER_ID_1, project);
        Assert.assertEquals(project, projectRepository.removeOneById(USER_ID_1, project.getId()));
        Assert.assertNull(projectRepository.findOneById(project.getId()));
    }

    @Test
    public void testRemoveOneByIdForUser() throws Exception {
        @NotNull ProjectDTO project = new ProjectDTO();
        project.setName(TEST_NAME);
        project.setDescription(TEST_DESCRIPTION);
        projectRepository.add(USER_ID_1, project);
        Assert.assertEquals(project, projectRepository.removeOneById(USER_ID_1, project.getId()));
        Assert.assertNull(projectRepository.findOneById(USER_ID_1, project.getId()));
        Assert.assertNull(projectRepository.findOneById(project.getId()));
    }

    @Test
    public void testRemoveOneByIndex() throws Exception {
        @NotNull ProjectDTO project = new ProjectDTO();
        project.setName(TEST_NAME);
        project.setDescription(TEST_DESCRIPTION);
        projectRepository.add(project);
        Assert.assertEquals(project, projectRepository.removeOneByIndex(USER_ID_1, NUMBER_OF_ENTRIES));
        Assert.assertNull(projectRepository.findOneById(project.getId()));
    }

    @Test
    public void testRemoveOneByIndexForUser() throws Exception {
        @NotNull ProjectDTO project = new ProjectDTO();
        project.setName(TEST_NAME);
        project.setDescription(TEST_DESCRIPTION);
        projectRepository.add(USER_ID_1, project);
        Assert.assertEquals(project, projectRepository.removeOneByIndex(USER_ID_1, NUMBER_OF_ENTRIES / 2 + 1));
        Assert.assertNull(projectRepository.findOneById(project.getId()));
    }

    @Test
    public void testRemoveAllForUser() throws Exception {
        @NotNull final List<ProjectDTO> emptyList = new ArrayList<>();
        projectRepository.removeAll(USER_ID_1);
        Assert.assertEquals(emptyList, projectRepository.findAll(USER_ID_1));
        Assert.assertNotEquals(emptyList, projectRepository.findAll(USER_ID_2));
    }

    @Test
    public void testRemoveAll() throws Exception {
        final int expectedNumberOfEntries = 0;
        projectRepository.removeAll(USER_ID_1);
        Assert.assertEquals(expectedNumberOfEntries, (long) projectRepository.getSize(USER_ID_1));
    }

}

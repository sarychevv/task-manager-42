package ru.t1.sarychevv.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.dto.model.SessionDTO;

import java.util.List;

public interface ISessionRepository {

    @Update("UPDATE tm_session SET name = #{name}, date = #{date}, role = #{role} WHERE id = #{id}")
    void update(@NotNull SessionDTO session);

    @Insert("INSERT INTO tm_session (id, date, user_id, role)" +
            " VALUES (#{id}, #{date}, #{userId}, #{role})")
    SessionDTO add(@NotNull SessionDTO model);

    @Insert("INSERT INTO tm_session (id, date, user_id, role)" +
            " VALUES (#{id}, #{date}, #{userId}, #{role})")
    SessionDTO add(@NotNull @Param("userId") String userId, @NotNull SessionDTO model);

    @NotNull
    @Select("SELECT * FROM tm_session WHERE user_id = #{userId}")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    List<SessionDTO> findAll(@NotNull @Param("userId") String userId);

    @NotNull
    @Select("SELECT * FROM tm_session")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    List<SessionDTO> findAll();

    @Nullable
    @Select("SELECT * FROM tm_session WHERE id = #{id} AND user_id = #{userId}")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    SessionDTO findOneById(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Nullable
    @Select("SELECT * FROM tm_session WHERE id = #{id}")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    SessionDTO findOneById(@NotNull @Param("id") String id);

    @Nullable
    @Select("SELECT * FROM tm_session WHERE user_id = #{userId} LIMIT 1 OFFSET #{index}")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    SessionDTO findOneByIndex(@NotNull @Param("userId") String userId, @NotNull @Param("index") Integer index);

    @Delete("DELETE FROM tm_session WHERE user_id = #{userId}")
    void removeAll(@NotNull @Param("userId") String userId);

    @Nullable
    @Delete("DELETE FROM tm_session WHERE user_id = #{userId} AND id = #{id}")
    SessionDTO removeOne(@NotNull SessionDTO session);

    @Nullable
    @Delete("DELETE FROM tm_session WHERE user_id = #{userId} AND id = #{id}")
    SessionDTO removeOneById(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Nullable
    @Delete("DELETE FROM tm_session WHERE id = #{id}")
    SessionDTO removeOneById(@NotNull @Param("id") String id);

    @Nullable
    @Delete("DELETE FROM tm_session WHERE user_id = #{userId} AND id = (SELECT id FROM tm_session WHERE user_id = #{userId} LIMIT 1 OFFSET #{index})")
    SessionDTO removeOneByIndex(@NotNull @Param("userId") String userId, @NotNull @Param("index") Integer index);

    @NotNull
    @Select("SELECT COUNT(*) FROM tm_session WHERE user_id = #{userId}")
    Integer getSize(@NotNull @Param("userId") String userId);

}

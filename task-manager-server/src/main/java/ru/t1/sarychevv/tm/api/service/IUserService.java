package ru.t1.sarychevv.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.dto.model.UserDTO;
import ru.t1.sarychevv.tm.enumerated.Role;

public interface IUserService {

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password) throws Exception;

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password, @Nullable String email) throws Exception;

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password, @Nullable Role role) throws Exception;

    @Nullable UserDTO removeByLogin(@Nullable String login) throws Exception;

    @Nullable
    UserDTO findByLogin(@Nullable String login) throws Exception;

    @Nullable
    UserDTO removeOne(@Nullable UserDTO model) throws Exception;

    @Nullable
    UserDTO setPassword(@Nullable String id, @Nullable String password) throws Exception;

    @Nullable
    UserDTO updateUser(@Nullable String id, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName) throws Exception;

    @NotNull
    Boolean isLoginExist(@Nullable String login) throws Exception;

    @NotNull
    Boolean isEmailExist(@Nullable String email) throws Exception;

    @NotNull
    UserDTO lockUserByLogin(@Nullable String login) throws Exception;

    @NotNull
    UserDTO unlockUserByLogin(@Nullable String login) throws Exception;

    @NotNull
    UserDTO findOneById(@Nullable String userId);
}

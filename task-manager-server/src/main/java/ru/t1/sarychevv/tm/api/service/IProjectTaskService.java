package ru.t1.sarychevv.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.dto.model.TaskDTO;

public interface IProjectTaskService {

    @Nullable
    TaskDTO bindTaskToProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId) throws Exception;

    @Nullable TaskDTO unbindTaskFromProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId) throws Exception;

    void removeProjectById(@Nullable String userId, @Nullable String projectId) throws Exception;

}

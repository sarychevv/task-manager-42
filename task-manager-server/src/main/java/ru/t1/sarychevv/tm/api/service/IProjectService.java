package ru.t1.sarychevv.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.dto.model.ProjectDTO;
import ru.t1.sarychevv.tm.enumerated.Status;

import java.util.Comparator;
import java.util.List;

public interface IProjectService {

    @Nullable
    ProjectDTO create(@Nullable String name, @Nullable String description) throws Exception;

    @Nullable
    ProjectDTO create(@Nullable String userId, @Nullable String name, @Nullable String description) throws Exception;

    @Nullable
    ProjectDTO updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description) throws Exception;

    @Nullable
    ProjectDTO updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description) throws Exception;

    @Nullable
    ProjectDTO changeStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status) throws Exception;

    @Nullable
    ProjectDTO changeStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status) throws Exception;

    boolean existsById(@Nullable String userId, @Nullable String id) throws Exception;

    @Nullable
    List<ProjectDTO> findAll(@Nullable String userId) throws Exception;

    @Nullable
    List<ProjectDTO> findAll(@Nullable String userId, @Nullable Comparator<ProjectDTO> comparator) throws Exception;

    @Nullable
    ProjectDTO findOneById(@Nullable String userId, @Nullable String id) throws Exception;

    @Nullable
    ProjectDTO findOneByIndex(@Nullable String userId, @Nullable Integer index) throws Exception;

    int getSize(@Nullable String userId) throws Exception;

    void removeAll(@Nullable String userId) throws Exception;

    @Nullable
    ProjectDTO removeOne(@Nullable String userId, @Nullable ProjectDTO model) throws Exception;

    @Nullable
    ProjectDTO removeOneById(@Nullable String userId, @Nullable String id) throws Exception;

    @Nullable
    ProjectDTO removeOneByIndex(@Nullable String userId, @Nullable Integer index) throws Exception;

    @Nullable
    ProjectDTO add(@Nullable String userId, @Nullable ProjectDTO model) throws Exception;

}
